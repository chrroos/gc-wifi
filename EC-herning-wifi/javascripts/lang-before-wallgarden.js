var locale = [

    {
        'id': 'title',
        'da': 'Velkommen til Politikens gratis&nbsp;wifi',
        'en': 'Welcome to the free Politiken&nbsp;wifi'
    },
    {
        'id': 'subtitle',
        'da': 'Nu kan du få de seneste nyheder fra <a href="http://hotspot.globalconnect.dk/redirect/politiken.dk/">politiken.dk</a> og finde inspiration til din dag i København på <a href="http://hotspot.globalconnect.dk/redirect/www.ibyen.dk/">ibyen.dk</a>.',
        'en': 'Now you can get the latest news from <a href="http://hotspot.globalconnect.dk/redirect/politiken.dk/">politiken.dk</a> and get inspired for day in Copenhagen with  <a href="http://hotspot.globalconnect.dk/redirect/www.ibyen.dk/">ibyen.dk</a>.'
    },
    {   'id': 'p1',
        'da': 'Politikens åbne wifi er tilgængeligt i området omkring Rådhuspladsen og er helt gratis. Netværket er leveret i samarbejde med GlobalConnect A/S. <br> God fornøjelse',
        'en': 'The wifi is available in the area around the City Hall Square and it is all free. The network is delivered in collaboration with GlobalConnect A/S.<br> Enjoy'
    },
    {
        'id': 'p2',
        'da': 'Hvis du vil videre ud på internettet med Politikens gratis wifi, skal du registrere dig her. Det er simpelt og hurtigt via sms.',
        'en': 'If you want to continue browsing the world wide web with the free Politiken wifi you will need to register here. It is quick and easy via sms.'
    },
    {
        'id': 'onlineTitle',
        'da': 'Du er på nettet',
        'en': 'You are online'
    },
    {
        'id': 'onlineP1',
        'da': 'Politikens gratis wifi er åbent for dig. God fornøjelse.',
        'en': 'The free Politiken wifi is all yours. Take it away.'
    },
    {
        'id': 'CheckPhoneNbHeader',
        'da': 'Start med at indtaste dit mobilnummer - du vil på dette nummer om lidt modtage en SMS med pinkode til login.',
        'en': 'Start by entering your mobile phone number - you will on this number shortly receive an SMS with a code to login.'
    },
    {
        'id': 'homeLink',
        'da': 'Tilbage til log ind',
        'en': 'Back to login'
    },
    {
        'id': 'CheckPhoneNbButton',
        'da': 'Start',
        'en': 'Start'
    },
    {
        'id': 'RegisterHeader',
        'da': 'Indtast venligst følgende oplysninger:',
        'en': 'Please enter the following information:'
    },
     {
        'id': 'RegisterHeaderFound',
        'da': 'Du kan tilrette dine informationer, hvis det er nødvendigt:',
        'en': 'You can edit your information, if necessary.:'
    },
    {
        'id': 'LoginHeader',
        'da': 'Login',
        'en': 'Login'
    },
    {
        'id': 'fLoginMobileNumberL',
        'da': 'Mobilnummer:',
        'en': 'Mobile phone number'
    },
    {
        'id': 'fLoginPasswordL',
        'da': 'Pinkode:',
        'en': 'Pin code'
    },

    {
        'id': 'loginButton',
        'da': 'Log ind',
        'en': 'Login'
    },
    {
        'id': 'foot1',
        'da': 'WiFi service leveret af <a href="http://hotspot.globalconnect.dk/redirect/www.globalconnect.dk/">GlobalConnect</a>',
        'en': 'WiFi service provided by <a href="http://hotspot.globalconnect.dk/redirect/www.globalconnect.dk/">GlobalConnect</a>'
    },
    {
        'id': 'foot2',
        'da': 'GlobalConnect er Danmarks førende alternative udbyder af løsninger til effektiv og sikker data- og telekommunikation samt housing.',
        'en': 'GlobalConnect is the leading alternative provider of solutions for efficient and secure data and telecommunications, and tele housing.'
    },
    {
        'id': 'registerNow',
        'da': 'Gratis registering',
        'en': 'Free registration'
    },
    {
        'id': 'orDivider',
        'da': 'eller log ind',
        'en': 'or login'
    },
    {
        'id': 'requestNewPw',
        'da': 'Glemt din pinkode?',
        'en': 'Forgot your pin code?'
    },
    {
        'id': 'fPasswordL',
        'da': 'Pinkode',
        'en': 'Pin code'
    },
    {
        'id': 'fPhoneNumberL',
        'da': 'Telefonnummer',
        'en': 'Phone number'
    },
    {
        'id': 'fMobileNumberL',
        'da': 'Mobilnummer',
        'en': 'Mobile phone number'
    },
    {
        'id': 'fFirstNameL',
        'da': 'Fornavn',
        'en': 'First name'
    },
    {
        'id': 'fLastNameL',
        'da': 'Efternavn',
        'en': 'Last name'
    },
    {
        'id': 'fRoadNameL',
        'da': 'Adresse',
        'en': 'Address'
    },
    {
        'id': 'fPostalCodeL',
        'da': 'Postnummer',
        'en': 'Zip code'
    },
    {
        'id': 'fCityL',
        'da': 'By',
        'en': 'City'
    },
    {
        'id': 'fCountryL',
        'da': 'Land',
        'en': 'Country'
    },
    {
        'id': 'fEmailL',
        'da': 'E-mail',
        'en': 'E-mail'
    },
    {
        'id': 'fReceiveMailL',
        'da': 'Modtag nyhedsbrev?',
        'en': 'Subscribe to our newletter?'
    },
    {
        'id': 'fTermsL',
        'da': 'Jeg accepterer <span showContainer="#termsText_container" class="jq_link">vilkår og betingelser</span>',
        'en': 'I accept <span showContainer="#termsText_container" class="jq_link"> terms and conditions </ span>'
    },
    {
        'id': 'RegisterButton',
        'da': 'Opret mig',
        'en': 'Register'
    },
    {
        'id': 'registerComplete',
        'da': 'Du vil snarest modtage en SMS med en 4-cifret pinkode, som du skal bruge sammen med dit mobilnummer for at logge ind.',
        'en': 'You will shortly receive an SMS with a pin code that you must use with your mobile phone number to login.'
    },
    {
        'id': 'registerCompleteBackToLogin',
        'da': 'Tilbage',
        'en': 'Back'
    },
    {
        'id': 'termsText',
        'da': '<div class="header">Vilkår og betingelser </div>Denne wi-fi internetforbindelse må ikke anvendes til nogen form for kriminel aktivitet, eller misbruges på anden vis.<br> Vi gør opmærksom på, at i henhold til dansk lovgivning om terrorlogning, bliver denne wi-fi forbindelse til internettet overvåget.',
        'en': '<div class="header">Terms and conditions </div>This Internet access must not be used for any criminal activity, or abused in some other way. <br> Please note that under Danish legislation on terrorism logging, this wi-fi connection to the internet is being monitored.'
    },
    {
        'id': 'closeTermsText',
        'da': 'Luk',
        'en': 'Close'
    },
    {
        'id': 'forgotPW',
        'da': 'Indtast det mobilnummer, som du allerede har registreret - du vil herefter modtage en SMS med en ny pinkode',
        'en': 'Enter the mobile phone number you already have registered - you will then receive a text message with a new pin code'
    },
    {
        'id': 'resetPWButton',
        'da': 'Ny pinkode',
        'en': 'New pin code'
    },
    {
        'id': 'forgotPWComplete',
        'da': 'Du vil snarest modtage en SMS med en ny pikode, som du skal bruge til login',
        'en': 'You will shortly receive an SMS with a new pin code, that you must use to Login'
    },
    {
        'id': 'forgotPWCompleteBackToLogin',
        'da': 'Tilbage',
        'en': 'Back'
    }
]

var locale_str = {
    'must_agree': {
        'da': 'Du skal accepterer vilkår og betingelser',
        'en': 'You must accept the terms and conditions'
    }
}

var dialogs = {
    'errorGeneric': {
        'header': {
            'da': 'Fejl opstået!',
            'en': 'Error!'
        },
        'body': {
            'da': 'Der er sket en uventet fejl <br> Prøv venligst igen',
            'en': 'There has been an unexpected error <br> Please try again'
        },
        'buttons': [
            {
                'action': '#login_container',
                'da': 'Tilbage',
                'en': 'Back'
            }
        ]
    },
    'userExist': {
        'header': {
            'da': 'Mobilnummer findes allerede i vores system!',
            'en': 'Mobile phone number already exists in our archives!'
        },
        'body': {
            'da': 'Hvis du har glemt din pinkode klik på "Glemt pinkode" - ellers gå til "Log ind"',
            'en': 'If you have forgotten your pin code click "Forgot pin code" - otherwise go to "Login"'
        },
        'buttons': [
            {
                'action': '#forgotPW_container',
                'da': 'Glemt pinkode',
                'en': 'Forgot pin code'
            },
            {
                'action': '#login_container',
                'da': 'Log ind',
                'en': 'Login'
            }
        ]
    },
    'userNotFound': {
        'header': {
            'da': 'Fejl opstået!',
            'en': 'Error!'
        },
        'body': {
            'da': 'Mobilnummer er ikke registreret - eller indtastet forkert. <br> Prøv venligst igen',
            'en': 'Mobile phone number is not registered - or entered incorrectly. <br> Please try again'
        },
        'buttons': [
            {
                'action': '#login_container',
                'da': 'Tilbage',
                'en': 'Back'
            }
        ]
    },

}

var locale_validator = {
    'da': {
        required: "Dette felt er påkrævet",
        maxlength: jQuery.validator.format("Indtast højst {0} tegn"),
        minlength: jQuery.validator.format("Indtast mindst {0} tegn"),
        rangelength: jQuery.validator.format("Indtast mindst {0} og højst {1} tegn"),
        email: "Indtast en gyldig email-adresse",
        url: "Indtast en gyldig URL",
        date: "Indtast en gyldig dato",
        number: "Indtast et tal",
        digits: "Indtast kun cifre",
        equalTo: "Indtast den samme værdi igen",
        range: jQuery.validator.format("Angiv en værdi mellem {0} og {1}"),
        max: jQuery.validator.format("Angiv en værdi der højst er {0}"),
        min: jQuery.validator.format("Angiv en værdi der mindst er {0}"),
        creditcard: "Indtast et gyldigt kreditkortnummer"
    },
    'en': {
        required: "This field is required",
        remote: "Please fix this field",
        email: "Please enter a valid email address",
        url: "Please enter a valid URL",
        date: "Please enter a valid date",
        dateISO: "Please enter a valid date (ISO)",
        number: "Please enter a valid number",
        digits: "Please enter only digits",
        creditcard: "Please enter a valid credit card number",
        equalTo: "Please enter the same value again",
        maxlength: $.validator.format("Please enter no more than {0} characters"),
        minlength: $.validator.format("Please enter at least {0} characters"),
        rangelength: $.validator.format("Please enter a value between {0} and {1} characters long"),
        range: $.validator.format("Please enter a value between {0} and {1}"),
        max: $.validator.format("Please enter a value less than or equal to {0}"),
        min: $.validator.format("Please enter a value greater than or equal to {0}")
    }
}

